//
//  AppDelegate.m
//  HeartSight
//
//  Created by Eric Betts on 4/25/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [NewRelic enableFeatures:NRFeatureFlag_HttpResponseBodyCapture];
    [NewRelicAgent startWithApplicationToken:HS_APP_TOKEN];

    // Override point for customization after application launch.

    //create/get your HKHealthStore instance (called healthStore here)
    self.healthStore = [[HKHealthStore alloc] init];

    //get permission to read the data types you need.
    [self getHeartratePermission:application];

    return YES;
}

- (void)startBackgroundSampling:(UIApplication *)application {
    NR_TRACE_METHOD_START(0);
    HKQuantityType *type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    void (^enableCompletion)(BOOL, NSError *) = ^(BOOL success, NSError *error) {
      NR_TRACE_METHOD_START(0);
      if (success) {
          //NSLog(@"background delivery enable successful");
      } else if (error) {
          NSLog(@"background delivery enable error: %@", error);
      } else {
          NSLog(@"background delivery enable not successful, but no error given");
      }
      NR_TRACE_METHOD_STOP;
    };

    [self.healthStore enableBackgroundDeliveryForType:type frequency:HKUpdateFrequencyImmediate withCompletion:enableCompletion];

    void (^sampleUpdateHandler)(HKObserverQuery *, HKObserverQueryCompletionHandler, NSError *) = ^void(HKObserverQuery *query, HKObserverQueryCompletionHandler completionHandler, NSError *error) {
      NR_TRACE_METHOD_START(0);
      //NSLog(@"sampleUpdate %@", [self formatDate:[NSDate date]]);
      if (error) {
          NSLog(@"sampleUpdate error %@", error.localizedDescription);
          abort();
      }

      [self queryHeartrate:completionHandler];
      NR_TRACE_METHOD_STOP;
    };

    HKQuery *query = [[HKObserverQuery alloc] initWithSampleType:type predicate:nil updateHandler:sampleUpdateHandler];
    [self.healthStore executeQuery:query];
    NR_TRACE_METHOD_STOP
}

#define NO_LIMIT 0

- (void)queryHeartrate:(HKObserverQueryCompletionHandler)completionHandler {
    NR_TRACE_METHOD_START(0);
    //NSLog(@"queryHeartrate");
    HKQuantityType *type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:HKSampleSortIdentifierStartDate ascending:YES];

    void (^queryResultHandler)(HKSampleQuery *, NSArray *, NSError *) = ^void(HKSampleQuery *query, NSArray *results, NSError *error) {
      NR_TRACE_METHOD_START(0);
      if (error) {
          NSLog(@"An error occured fetching the user's heart rate. In your app, try to handle this gracefully. The error was: %@.", error);
          abort();
      } else if (results) {
          [NewRelic recordEvent:@"HeartRateSamples"
                     attributes:@{
                         @"count" : [NSNumber numberWithInteger:results.count]
                     }];
          [NewRelic recordMetricWithName:@"HeartRateSample" category:@"HKSampleQuery" value:[NSNumber numberWithInteger:results.count]];
          NSMutableArray *events = [@[] mutableCopy];
          if (events) {
            //http://stackoverflow.com/questions/6195294/objective-c-what-is-the-fastest-and-most-efficient-way-to-enumerate-an-array
            [results enumerateObjectsWithOptions:NSEnumerationConcurrent
                                      usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                        HKQuantitySample *sample = (HKQuantitySample *)obj;
                                        NSDictionary *event = [self buildEvent:sample];
                                        [events addObject:event];
                                      }];
            [self sendToInsights:events andComplete:completionHandler];
          }
      }
      NR_TRACE_METHOD_STOP;
    };

    HKSampleQuery *dataQuery = [[HKSampleQuery alloc] initWithSampleType:type predicate:self.recent limit:NO_LIMIT sortDescriptors:@[ sortDescriptor ] resultsHandler:queryResultHandler];

    [self.healthStore executeQuery:dataQuery];
    NR_TRACE_METHOD_STOP;
}

- (NSDictionary *)buildEvent:(HKQuantitySample *)sample {
    NR_TRACE_METHOD_START(0);
    HKUnit *heartRate = [HKUnit.countUnit unitDividedByUnit:HKUnit.minuteUnit];
    double bpm = [sample.quantity doubleValueForUnit:heartRate];
    NSNumber *rate = [NSNumber numberWithDouble:bpm];
    NSNumber *timestamp = [NSNumber numberWithInt:[sample.startDate timeIntervalSince1970]];
    //NSLog(@"%@: %@ bpm", [self formatDate:sample.startDate], rate);

    NSDictionary *event = @{
        @"eventType" : @"HeartRate",
        @"timestamp" : timestamp,
        @"rate" : rate
    };
    NR_TRACE_METHOD_STOP;
    return event;
}

- (void)sendToInsights:(NSArray *)events andComplete:(HKObserverQueryCompletionHandler)completionHandler {
    NR_TRACE_METHOD_START(0);
    //http://codewithchris.com/tutorial-how-to-use-ios-nsurlconnection-by-example/#post

    NSURL *insightsUrl = [NSURL URLWithString:MOBILE_STAGING_INSIGHTS_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:insightsUrl];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:HS_STAGING_INSIGHTS_INSERT forHTTPHeaderField:@"X-Insert-Key"];

    NSError *err;
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:events options:0 error:&err];

    void (^requestComplete)(NSURLResponse *response, NSData *data, NSError *connectionError) = ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
      NR_TRACE_METHOD_START(0);
      //NSLog(@"Request completed: %li", ((NSHTTPURLResponse *)response).statusCode);
      if (completionHandler) {
          completionHandler();
      }
      NR_TRACE_METHOD_STOP;
    };

    if (events.count > 0) {
        NSDictionary *lastEvent = events[events.count - 1];
        NSNumber *lastTs = (NSNumber *)lastEvent[@"timestamp"];
        NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:[lastTs doubleValue]];
        NSLog(@"Sending %lu events.  Most recent was at %@", events.count, [self formatDate:lastDate]);
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:requestComplete];
    } else {
        NSLog(@"No events to send");
        if (completionHandler) {
            completionHandler();
        }
    }
    NR_TRACE_METHOD_STOP;
}

- (NSPredicate *)recent {
    NR_TRACE_METHOD_START(0);
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitHour value:24 toDate:now options:0];
    NSDate *startDate = [calendar dateByAddingUnit:NSCalendarUnitHour value:-24 toDate:now options:0];

    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionNone];
    NR_TRACE_METHOD_STOP;
    return predicate;
}

- (NSString *)formatDate:(NSDate *)date {
    NR_TRACE_METHOD_START(0);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    dateFormatter.dateStyle = NSDateFormatterShortStyle;

    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    NR_TRACE_METHOD_STOP;
    return [dateFormatter stringFromDate:date];
}

- (void)getHeartratePermission:(UIApplication *)application {
    NR_TRACE_METHOD_START(0);
    HKQuantityType *type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];

    void (^authorizationHandler)(BOOL success, NSError *error) = ^void(BOOL success, NSError *error) {
      NR_TRACE_METHOD_START(0);
      if (success) {
          //NSLog(@"Heart rate authorized");
          [self startBackgroundSampling:application];
      } else {
          NSLog(@"Doesn't have access to heartrate: %@", error.localizedDescription);
          return;
      }
      NR_TRACE_METHOD_STOP;
    };

    if ([HKHealthStore isHealthDataAvailable]) {
        //NSLog(@"Health data is available");
        NSSet *readDataTypes = [NSSet setWithObject:type];
        [self.healthStore requestAuthorizationToShareTypes:nil readTypes:readDataTypes completion:authorizationHandler];
    }
    NR_TRACE_METHOD_STOP;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
