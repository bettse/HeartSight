//
//  AppDelegate.h
//  HeartSight
//
//  Created by Eric Betts on 4/25/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

@import Foundation;
#import <UIKit/UIKit.h>
#import <HealthKit/HealthKit.h>
#include "keys.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HKHealthStore *healthStore;

@end
