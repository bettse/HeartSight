//
//  ViewController.m
//  HeartSight
//
//  Created by Eric Betts on 4/25/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sample:) name:@"sample" object:nil];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beat)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;

    self.heart.userInteractionEnabled = YES;
    [self.heart addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sample:(NSNotification *)notification {
    [self beat];
}

- (void)beat {
    /* https://gist.github.com/anka/1b93b47ab40e03b82f6d#file-gistfile1-m */
    //Create an animation with pulsating effect
    CABasicAnimation *theAnimation;

    //within the animation we will adjust the "opacity"
    //value of the layer
    theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    //animation lasts 0.4 seconds
    theAnimation.duration = 1.0;
    //and it repeats forever
    theAnimation.repeatCount = 1;
    //we want a reverse animation
    theAnimation.autoreverses = YES;
    //justify the opacity as you like (1=fully visible, 0=unvisible)
    theAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    theAnimation.toValue = [NSNumber numberWithFloat:0.5];

    //Assign the animation to your UIImage layer and the
    //animation will start immediately
    [self.heart.layer addAnimation:theAnimation forKey:@"animateOpacity"];
}

@end
