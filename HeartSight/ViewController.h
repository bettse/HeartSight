//
//  ViewController.h
//  HeartSight
//
//  Created by Eric Betts on 4/25/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
}

@property (weak, nonatomic) IBOutlet UIImageView *heart;

@end
